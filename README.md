# README #

OptiTrack's NatNet API implementation on Qt (cross platform) for Linux and Windows

### What is this repository for? ###

#### Quick summary ####
This repo is useful for you if you are making motion capture with NaturalPoint's OptiTrack® software (any OptiTrack® streaming server using the NatNet protocol, like Motive), and you want to get the motion capture data streaming on Linux or Windows, in real time, using the Qt Framework and the NatNet's API.

### Building ###

##### Dependencies #####
* Qt 5.0 or later
* qmake 3.0 or later

##### Building instructions #####
Just git clone this repository, make a building directory within it, execute qmake inside this building directory 
using the directory where NatNet_Client.pro Qt project file is located as the source, and finally execute make.

Just like this (instructions for Linux):
```
git clone git@bitbucket.org:lcgonzalez/natnet_qt.git
mkdir build
cd build
qmake ../
make
```

### Contributions and repo owner ###

##### Repo owner or admin #####
Any suggestions, contributions or issues, please contact the repo owner:

*Luis González: lc.gonzalez23@gmail.com*
