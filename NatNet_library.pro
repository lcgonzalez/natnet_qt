## Copyright (C) 2015 Luis Carlos Gonzalez Garcia. All rights reserved.
##**
##  @file natnet.cpp
##  @author Luis Gonzalez <lc.gonzalez23@gmail.com>
##  @version 1.0
##  @section LICENSE
##  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
##
##    This file is part of NatNet Qt Library.
##
##                          License Agreement
##                     For the NatNet Qt Library
##
##    NatNet Qt Library is a library that implements NatNet's API in Qt,
##    allowing cross platform NatNet servers and clients (Linux and Windows)
##    to use NatNet's API to communicate among them.
##    Copyright (C) 2015 Luis Carlos Gonzalez Garcia
##
##    NatNet Qt Library is free software: you can redistribute it and/or
##    modify it under the terms of the GNU General Public License as published
##    by the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    NatNet Qt Library is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.
##*/

#-------------------------------------------------
#
# Project created by QtCreator 2015-05-12T14:08:08
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = NatNet_library
TEMPLATE = lib

DEFINES += NATNET_LIBRARY_LIBRARY

SOURCES += natnet.cpp

HEADERS += natnet.h\
        NatNet_library_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
